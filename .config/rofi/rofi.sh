#!/usr/bin/env bash
options="Search
Network
Bluetooth
SSH"
theme=${1:-$HOME/.config/rofi/config.rasi}
selection=$(echo -e "${options}" | rofi -dmenu -config $theme)
case "${selection}" in
  "Network")
    exec ~/.config/rofi/menu/iwd.sh;;
  "Bluetooth")
    exec ~/.config/rofi/menu/bt.sh;;
  "Search")
    rofi -show drun -show-icons -modi drun,run;;
  "SSH")
    exec ~/.config/rofi/menu/ssh.sh;;
esac
