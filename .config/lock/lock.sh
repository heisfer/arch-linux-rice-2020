#!/bin/sh
i3lock -b -i ~/.config/lock/lock.png  -p win -e -f -k --bar-indicator --bar-base-width=1  --bar-direction=2 --veriftext="Uploading data..." --wrongtext="Pipe broke! Try again later." -C --timestr="%I:%M:%S %p" --composite --pass-media-keys -u

